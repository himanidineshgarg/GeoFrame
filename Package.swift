// swift-tools-version: 5.5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "GeoFrame",
    platforms:[
        .iOS(.v14)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "GeoFrame",
            targets: ["GeoFrame"]),
    ],
    dependencies: [
   
    ],
    targets: [
                .binaryTarget(name: "GeoFrame", path: "./Sources/GeoFrame.xcframework")
            ]
)
