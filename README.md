# GeoFrame

A description of this package.

Swift Package Manager

In Xcode, select File > Swift Packages > Add Package Dependency.

By using the following link:-
https://gitlab.com/himanidineshgarg/GeoFrame

Follow the prompts using the URL for this repository
Select the Geo Frame and you can easily use in inside any project.


How to use

1. import GeoFrame in project

2. After import follow the below steps to include the Geotourist app


    let vc = GeoFrame.Utility.performLogin(googleKey: google_map_key, tourID: tour_id)
    
    self.navigationController?.pushViewController(vc, animated: true)

//Here google_map_key is the key provided by google maps platform

//Here tour_id is the id of any specific tour that is registered on Geotourist server 

3. To perform the logout operation,

    Utility.Logout() 
             
